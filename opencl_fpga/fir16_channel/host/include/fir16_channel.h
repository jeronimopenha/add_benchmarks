#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <chrono>
// ACL specific includes
#include "CL/opencl.h"
#include "AOCLUtils/aocl_utils.h"

using namespace aocl_utils;
using namespace std;
using namespace std::chrono;

const size_t V = 32;
size_t vectorSize = 1000000;
char *kernel_name[3] = {
 "reader",
 "fir16",
 "writer"
};

double bw;

// ACL runtime configuration
cl_platform_id platform;
cl_device_id device;
cl_context context;
cl_command_queue queue;
cl_kernel kernel;
cl_kernel kernel_read;
cl_kernel kernel_write;
cl_program program;
cl_int status;

// input and output vectors
unsigned short *hdin,*hcoef, *hdout;

void initializeVector(unsigned short* vector, unsigned short data, int size);

void initializeVector_seq(unsigned short* vector, int size);

void initializeVector_seq_inv(unsigned short* vector, int size);

void dump_error(const char *str, cl_int status);

// free the resources allocated during initialization
void freeResources();

void cleanup();

int main(int argc, char *argv[]);
