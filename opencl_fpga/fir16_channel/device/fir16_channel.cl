#pragma OPENCL EXTENSION cl_altera_channels : enable

channel unsigned short reader_out __attribute__((depth(16)));
channel unsigned short fir_out __attribute__((depth(16)));

__kernel void
__attribute__((task))
reader( __global unsigned short * restrict din,
         int size
){
        for (int i = 0; i < size; i++) {
            unsigned short tmp;
            tmp = din[i];
            write_channel_altera(reader_out,tmp); 
        }    
}

__kernel void 
__attribute__((task))
fir16( __global const unsigned short * restrict coef,
          int n       
){
     unsigned short ai[16];
     unsigned short coef_hw[16];
     #pragma unroll 16
     for(int i = 0; i < 16; i++) {
        coef_hw[i] = coef[i];       
     }
     for (int i=0; i < n; i++){
         unsigned short fir = 0;
         ai[0] = read_channel_altera(reader_out);
         #pragma unroll 15
         for (int k=15; k > 0; k--){
             fir += ai[k] * coef_hw[k];
             ai[k] = ai[k-1];
         }
         
         fir += ai[0] * coef_hw[0];
         
         if (i >= 15){
             write_channel_altera(fir_out,fir);    
         }
     }
}

__attribute__((task))
__kernel
void encrypt( __global unsigned short * restrict dout,
              int size
){
        
        for (int i = 0; i < size; i++) {
            unsigned short tmp;
            tmp = read_channel_altera(fir_out);
            dout[i] = tmp;
        }
}


