
#pragma OPENCL EXTENSION cl_altera_channels : enable

channel unsigned int decrypt_v0_out,decrypt_v1_out;
channel unsigned int fir0_out,fir1_out;


__kernel void
__attribute__((task))
decrypt( __global unsigned int * restrict v0_in,
         __global unsigned int * restrict v1_in,
         const unsigned int k0,
         const unsigned int k1,
         const unsigned int k2,
         const unsigned int k3,
         int size
){
    
        for (int i = 0; i < size; i++) {
            unsigned int sum = 0xC6EF3720, delta = 0x9e3779b9;
            unsigned int v0_tmp,v1_tmp;
            v0_tmp = v0_in[i];
            v1_tmp = v1_in[i];
            #pragma unroll 32
            for (int j = 0; j < 32; j++) {
                v1_tmp -= ((v0_tmp << 4) + k2) ^ (v0_tmp + sum) ^ ((v0_tmp >> 5) + k3);
                v0_tmp -= ((v1_tmp << 4) + k0) ^ (v1_tmp + sum) ^ ((v1_tmp >> 5) + k1);
                sum -= delta;
            }
            write_channel_altera(decrypt_v0_out,v0_tmp); 
            write_channel_altera(decrypt_v1_out,v1_tmp); 
        }    
}

__kernel void 
__attribute__((task))
fir16_0 ( __global const unsigned int * restrict coef,
          int n       
){
        
     unsigned int ai[16];
     unsigned int coef_hw[16];
     #pragma unroll 16
     for(int i = 0; i < 16; i++) {
        coef_hw[i] = coef[i];       
     }
     
     for (int i=0; i < n; i++){
         unsigned int fir = 0;
         ai[0] = read_channel_altera(decrypt_v0_out);
         #pragma unroll 15
         for (int k=15; k > 0; k--){
             fir += ai[k] * coef_hw[k];
             ai[k] = ai[k-1];
         }
         
         fir += ai[0] * coef_hw[0];
         
         if (i >= 15){
             write_channel_altera(fir0_out,fir);    
         }
     }
}

__kernel void 
__attribute__((task))
fir16_1 ( __global const unsigned int * restrict coef,
          int n       
){
        
     unsigned int ai[16];
     unsigned int coef_hw[16];
     #pragma unroll 16
     for(int i = 0; i < 16; i++) {
        coef_hw[i] = coef[i];       
     }
     
     for (int i=0; i < n; i++){
         unsigned int fir = 0;
         ai[0] = read_channel_altera(decrypt_v1_out);
         #pragma unroll 15
         for (int k=15; k > 0; k--){
             fir += ai[k] * coef_hw[k];
             ai[k] = ai[k-1];
         }
         
         fir += ai[0] * coef_hw[0];
         
         if (i >= 15){
             write_channel_altera(fir1_out,fir);    
         }
     }
}

__attribute__((task))
__kernel
void encrypt( __global unsigned int * restrict v0_out,
              __global unsigned int * restrict v1_out,
              const unsigned int k0,
              const unsigned int k1,
              const unsigned int k2,
              const unsigned int k3,
              int size
){
        
        for (int i = 0; i < size; i++) {
            unsigned int sum = 0, delta = 0x9e3779b9;
            unsigned int v0_tmp,v1_tmp;
            
            v0_tmp = read_channel_altera(fir0_out);
            v1_tmp = read_channel_altera(fir1_out);
            #pragma unroll 32  
            for (int j = 0; j < 32; j++) {
                sum += delta;
                v0_tmp += ((v1_tmp << 4) + k0) ^ (v1_tmp + sum) ^ ((v1_tmp >> 5) + k1);
                v1_tmp += ((v0_tmp << 4) + k2) ^ (v0_tmp + sum) ^ ((v0_tmp >> 5) + k3);
            }
            
            v0_out[i] = v0_tmp;
            v1_out[i] = v1_tmp;
            
        }
    
}


