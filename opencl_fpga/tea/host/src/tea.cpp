#include <tea.h>


static void initializeVector(unsigned int* vector, unsigned int data, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = data;
  }
}

static void initializeVector_seq(unsigned int* vector, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = i;
  }
}

static void initializeVector_seq_inv(unsigned int* vector, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = size- i;
  }
}

static void dump_error(const char *str, cl_int status) {
  printf("%s\n", str);
  printf("Error code: %d\n", status);
}

// free the resources allocated during initialization
static void freeResources() {

  for(int i=0;i < 4;i++){
    if(kernel[i]) 
    clReleaseKernel(kernel[i]);      
  }    
  if(program) 
    clReleaseProgram(program);
  
  for(int i=0;i < 4;i++){
  if(queue[i]) 
    clReleaseCommandQueue(queue[i]);     
  }
 
  if(v0_in) 
   clSVMFreeAltera(context,v0_in);
  if(v0_out) 
   clSVMFreeAltera(context,v0_out);
  if(v1_in) 
   clSVMFreeAltera(context,v1_in);
  if(v1_out) 
   clSVMFreeAltera(context,v1_out);
  if(coef)
   clSVMFreeAltera(context,coef);
  if(context) 
    clReleaseContext(context);
}

void cleanup(){}

int main(int argc, char *argv[]) {
    
  cl_uint num_platforms;
  cl_uint num_devices;
  int lines = vectorSize/V;
  if ( argc >= 2 ) /* argc should be  >2 for correct execution */
  {
      vectorSize = atoi(argv[1]);
      lines = atoi(argv[1])/V;
  }    
    
  // get the platform ID
  status = clGetPlatformIDs(1, &platform, &num_platforms);
  if(status != CL_SUCCESS) {
    dump_error("Failed clGetPlatformIDs.", status);
    freeResources();
    return 1;
  }
  
  if(num_platforms != 1) {
    printf("Found %d platforms!\n", num_platforms);
    freeResources();
    return 1;
  }

  // get the device ID
  status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 1, &device, &num_devices);
  if(status != CL_SUCCESS) {
    dump_error("Failed clGetDeviceIDs.", status);
    freeResources();
    return 1;
  }
  
  if(num_devices != 1) {
    printf("Found %d devices!\n", num_devices);
    freeResources();
    return 1;
  }

  // create a context
  context = clCreateContext(0, 1, &device, NULL, NULL, &status);
  if(status != CL_SUCCESS) {
    dump_error("Failed clCreateContext.", status);
    freeResources();
    return 1;
  }
    
  printf("Creating host buffers.\n");
  

  v0_in = (unsigned int*)clSVMAllocAltera(context, 0, vectorSize*sizeof(unsigned int), 1024); 
  v0_out = (unsigned int*)clSVMAllocAltera(context, 0, vectorSize*sizeof(unsigned int), 1024); 

  v1_in =  (unsigned int*)clSVMAllocAltera(context, 0, vectorSize*sizeof(unsigned int), 1024); 
  v1_out =  (unsigned int*)clSVMAllocAltera(context, 0, vectorSize*sizeof(unsigned int), 1024); 
  
  coef = (unsigned int*)clSVMAllocAltera(context, 0, 16*sizeof(unsigned int), 1024);
  
  double numBytes = vectorSize*sizeof(unsigned int)*4;
  
  if(!v0_in || !v0_out || !v1_in || !v1_out || !coef) {
     dump_error("Failed to allocate buffers.", status);
     freeResources();
     return 1;   
  }
  
  initializeVector(v0_in,2,vectorSize);
  initializeVector(v0_out,0,vectorSize);
  
  initializeVector(v1_in,2,vectorSize);
  initializeVector(v1_out,0,vectorSize);
  
  initializeVector_seq_inv(coef,16);
  
  // create a command queue
  for(int i=0;i < 4;i++){
        queue[i] = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
        if(status != CL_SUCCESS) {
            dump_error("Failed clCreateCommandQueue.", status);
            freeResources();
            return 1;
        }
  }  
  // create the program
  cl_int kernel_status;  
  size_t binsize = 0;
  unsigned char * binary_file = loadBinaryFile("../../device/tea.aocx", &binsize);
  if(!binary_file) {
    dump_error("Failed loadBinaryFile.", status);
    freeResources();
    return 1;
  }
  program = clCreateProgramWithBinary(context, 1, &device, &binsize, (const unsigned char**)&binary_file, &kernel_status, &status);
  if(status != CL_SUCCESS) {
    dump_error("Failed clCreateProgramWithBinary.", status);
    freeResources();
    return 1;
  }
  delete [] binary_file;
  // build the program
  status = clBuildProgram(program, 0, NULL, "", NULL, NULL);
  if(status != CL_SUCCESS) {
    dump_error("Failed clBuildProgram.", status);
    freeResources();
    return 1;
  }
  printf("Creating TEA + FIR16 kernel\n");
  // create the kernel
  for(int i=0;i<4;i++){
    kernel[i] = clCreateKernel(program,kernel_names[i], &status);
    if(status != CL_SUCCESS) {
        dump_error("Failed clCreateKernel.", status);
        freeResources();
        return 1;
    } 
  }
  
  high_resolution_clock::time_point s;
  duration<double> diff{};
  s = high_resolution_clock::now();
  
  // set the arguments decrypt:
  status = clSetKernelArgSVMPointerAltera(kernel[K_DECRYPT], 0, (void*)v0_in);
  if(status != CL_SUCCESS) {
    dump_error("Failed set decrypt arg 0.", status);
    return 1;
  }
  
  status = clSetKernelArgSVMPointerAltera(kernel[K_DECRYPT], 1, (void*)v1_in);
  if(status != CL_SUCCESS) {
    dump_error("Failed set decrypt arg 1.", status);
    freeResources();
    return 1;
  }
  
  for(int i = 0;i < 4;i++){
    cl_uint arg = key[i];
    status = clSetKernelArg(kernel[K_DECRYPT],i+2, sizeof(cl_uint), &(arg));
    if(status != CL_SUCCESS) {
        dump_error("Failed set decrypt arg.", status);
        freeResources();
        return 1;
    }
  }
  cl_int arg6 = vectorSize;
  status = clSetKernelArg(kernel[K_DECRYPT],6, sizeof(cl_int), &(arg6));
  if(status != CL_SUCCESS) {
        dump_error("Failed set decrypt arg 6.", status);
        freeResources();
        return 1;
  }
  
  //set arg fir16_0:
  status = clSetKernelArgSVMPointerAltera(kernel[K_FIR16_0], 0, (void*)coef);
  if(status != CL_SUCCESS) {
    dump_error("Failed Set fir16_0 0.", status);
    freeResources();
    return 1;
  }
  cl_int arg2 = vectorSize;
  status = clSetKernelArg(kernel[K_FIR16_0],1, sizeof(cl_int), &(arg2));
  if(status != CL_SUCCESS) {
        dump_error("Failed Set arg fir16_0 1.", status);
        freeResources();
        return 1;
  }
  
  //set arg fir16_1:
  status = clSetKernelArgSVMPointerAltera(kernel[K_FIR16_1], 0, (void*)coef);
  if(status != CL_SUCCESS) {
    dump_error("Failed Set fir16_1 0.", status);
    freeResources();
    return 1;
  }
  status = clSetKernelArg(kernel[K_FIR16_1],1, sizeof(cl_int), &(arg2));
  if(status != CL_SUCCESS) {
        dump_error("Failed Set arg fir16_1 1.", status);
        freeResources();
        return 1;
  }
  //set arg encrypt:
  status = clSetKernelArgSVMPointerAltera(kernel[K_ENCRYPT], 0, (void*)v0_out);
  if(status != CL_SUCCESS) {
    dump_error("Failed set encrypt arg 0.", status);
    return 1;
  }
  
  status = clSetKernelArgSVMPointerAltera(kernel[K_ENCRYPT], 1, (void*)v1_out);
  if(status != CL_SUCCESS) {
    dump_error("Failed set encrypt arg 1.", status);
    freeResources();
    return 1;
  }
  
  for(int i=0;i < 4;i++){
    cl_uint arg = key[i];
    status = clSetKernelArg(kernel[K_ENCRYPT],i+2, sizeof(cl_uint), &(arg));
    if(status != CL_SUCCESS) {
        dump_error("Failed set encrypt arg.", status);
        freeResources();
        return 1;
    }
  }
  arg6 = vectorSize-16;
  status = clSetKernelArg(kernel[K_ENCRYPT],6, sizeof(cl_int), &(arg6));
  if(status != CL_SUCCESS) {
        dump_error("Failed set encrypt arg 6.", status);
        freeResources();
        return 1;
  }  
  
  printf("Launching the kernel...\n");
  
  status = clEnqueueSVMMap(queue[K_DECRYPT], CL_TRUE, CL_MAP_READ | CL_MAP_WRITE,(void *)v0_in,vectorSize*sizeof(unsigned int), 0, NULL, NULL); 
  if(status != CL_SUCCESS) {
    dump_error("Failed clEnqueueSVMMap", status);
    freeResources();
    return 1;
  }
  status = clEnqueueSVMMap(queue[K_DECRYPT], CL_TRUE, CL_MAP_READ | CL_MAP_WRITE,(void *)v1_in,vectorSize*sizeof(unsigned int), 0, NULL, NULL); 
  if(status != CL_SUCCESS) {
    dump_error("Failed clEnqueueSVMMap", status);
    freeResources();
    return 1;
  }
  
  status = clEnqueueSVMMap(queue[K_FIR16_0], CL_TRUE,  CL_MAP_READ | CL_MAP_WRITE,(void *)coef,16, 0, NULL, NULL); 
  if(status != CL_SUCCESS) {
    dump_error("Failed clEnqueueSVMMap", status);
    freeResources();
    return 1;
  }   
  status = clEnqueueSVMMap(queue[K_FIR16_1], CL_TRUE,  CL_MAP_READ | CL_MAP_WRITE,(void *)coef,16, 0, NULL, NULL); 
  if(status != CL_SUCCESS) {
    dump_error("Failed clEnqueueSVMMap", status);
    freeResources();
    return 1;
  }   
  status = clEnqueueSVMMap(queue[K_ENCRYPT], CL_TRUE,  CL_MAP_READ | CL_MAP_WRITE,(void *)v0_out,vectorSize*sizeof(unsigned int), 0, NULL, NULL); 
  if(status != CL_SUCCESS) {
    dump_error("Failed clEnqueueSVMMap", status);
    freeResources();
    return 1;
  } 
  status = clEnqueueSVMMap(queue[K_ENCRYPT], CL_TRUE,  CL_MAP_READ | CL_MAP_WRITE,(void *)v1_out,vectorSize*sizeof(unsigned int), 0, NULL, NULL); 
  if(status != CL_SUCCESS) {
    dump_error("Failed clEnqueueSVMMap", status);
    freeResources();
    return 1;
  }
  
  for(int i= 0;i < 4;i++){
    status = clEnqueueTask(queue[i], kernel[i], 0, NULL, NULL);
    if (status != CL_SUCCESS) {
        dump_error("Failed to launch kernel.", status);
        freeResources();
        return 1;
    }
  }
  
  status = clEnqueueSVMUnmap(queue[K_ENCRYPT], (void *)v0_out, 0, NULL, NULL); 
  if(status != CL_SUCCESS) {
    dump_error("Failed clEnqueueSVMUnmap", status);
    freeResources();
    return 1;
  }   
  
  status = clEnqueueSVMUnmap(queue[K_ENCRYPT], (void *)v1_out, 0, NULL, NULL); 
  if(status != CL_SUCCESS) {
    dump_error("Failed clEnqueueSVMUnmap", status);
    freeResources();
    return 1;
  } 
  
  for(int i= 0;i < 4;i++){
       status = clFinish(queue[i]); 
       if(status != CL_SUCCESS) {
         dump_error("Failed clFinish", status);
         freeResources();
         return 1;
       }   
  }
  
  diff = high_resolution_clock::now() - s;
  printf("Execution Time: %.4f ms\n",diff.count() * 1000);
  double time = diff.count();    
  bw = numBytes / (time) ;
  printf("Processed %d unsigned int in %.4f s\n", vectorSize*2, time);
  printf("Read/Write Bandwidth = %.0f MB/s\n", bw/(1024*1024));
  printf("Kernel execution is complete.\n");
  
  freeResources();

  return 0;
}

