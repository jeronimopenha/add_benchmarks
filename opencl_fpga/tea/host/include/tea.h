#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <chrono>
// ACL specific includes
#include "CL/opencl.h"
//#include "ACLHostUtils.h"
#include "AOCLUtils/aocl_utils.h"

using namespace aocl_utils;
using namespace std;
using namespace std::chrono;

static const size_t V = 16;
static size_t vectorSize = 1000000;
static const char* kernel_names[4] =
{
  "decrypt",
  "fir16_0",
  "fir16_1",
  "encrypt"
};
enum KERNELS {
  K_DECRYPT,
  K_FIR16_0,
  K_FIR16_1,
  K_ENCRYPT,
  K_NUM_KERNELS
};
double bw;

// ACL runtime configuration
static cl_platform_id platform;
static cl_device_id device;
static cl_context context;
static cl_command_queue queue[4];
static cl_kernel kernel[4];
static cl_program program;
static cl_int status;

// input and output vectors

static unsigned int *v0_in,*v0_out, *v1_in,*v1_out,*coef;

static unsigned int key[4] = {0x9474B8E8, 0xC73BCA7D, 0x53239142, 0xf3c3121a};


static void initializeVector(unsigned int* vector, unsigned int data, int size);

static void initializeVector_seq(unsigned int* vector, int size);

static void initializeVector_seq_inv(unsigned int* vector, int size);

static void dump_error(const char *str, cl_int status);

// free the resources allocated during initialization
static void freeResources();

void cleanup();

int main(int argc, char *argv[]);
