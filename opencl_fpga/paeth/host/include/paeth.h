#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <chrono>
// ACL specific includes
#include "CL/opencl.h"
#include "AOCLUtils/aocl_utils.h"

using namespace aocl_utils;
using namespace std;
using namespace std::chrono;

static const size_t V = 32;
static size_t vectorSize = 1000000;
static char *kernel_name =  "paeth";
double bw;

// ACL runtime configuration
static cl_platform_id platform;
static cl_device_id device;
static cl_context context;
static cl_command_queue queue;
static cl_kernel kernel;
static cl_kernel kernel_read;
static cl_kernel kernel_write;
static cl_program program;
static cl_int status;

// input and output vectors
static unsigned short  *hdin_a,*hdin_b,*hdin_c, *hdout;

static void initializeVector(unsigned short* vector, unsigned short data, int size);

static void initializeVector_seq(unsigned short* vector, int size);

static void initializeVector_seq_inv(unsigned short* vector, int size);

static void dump_error(const char *str, cl_int status);

// free the resources allocated during initialization
static void freeResources();

void cleanup();

int main(int argc, char *argv[]);
