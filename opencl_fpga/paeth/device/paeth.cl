__kernel void 
__attribute__((task))
paeth (__global const unsigned short * restrict a,
       __global const unsigned short * restrict b,
       __global const unsigned short * restrict c,
       __global unsigned short * restrict out,
       int n
){
  
  for(int i = 0; i < n; i= i+32) {
      #pragma unroll 32
      for(int j = 0; j < 32; j++){
          int pas, pbs, pcs;
          bool test_1, test_2, test_3;
          pas = b[i*32 + j] - c[i*32 + j];
          pbs = a[i*32 + j] - c[i*32 + j];
          pcs = a[i*32 + j] | b[i*32 + j] - 2 * c[i*32 + j];
          test_1 = abs(pas) <= abs(pbs);
          test_2 = abs(pas) <= abs(pcs);
          test_3 = abs(pbs) <= abs(pcs);
          if (test_1 && test_2) out[i*32 + j] = a[i*32 + j];
          else if (test_3) out[i*32 + j] = b[i*32 + j];
          else out[i*32 + j] = c[i*32 + j];
      }
  }
}
