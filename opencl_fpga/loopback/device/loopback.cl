#pragma OPENCL EXTENSION cl_altera_channels : enable


typedef struct cache_line{
   unsigned short data[32]; 
}cl_t;

channel unsigned short fifo_data[32]__attribute__((depth(8)));

__attribute__((task))
__kernel 
void load_data(__global cl_t * restrict data_in, int n){
    
    for(int i = 0; i < n;i++){
        #pragma unroll 32
        for(int j=0; j < 32;j++){
            write_channel_altera(fifo_data[j],data_in[i].data[j]); 
        }
    }    
    
}


__attribute__((task))
__kernel
void store_data(__global cl_t * restrict data_out, int n){
    
    for(int i = 0; i < n;i++){
        #pragma unroll 32
        for(int j=0; j < 32;j++){
            data_out[i].data[j] = read_channel_altera(fifo_data[j]);
        }
    }    
}
