#include <loopback.h>


static void initializeVector(cl_t * vector, unsigned short data, int size) {
  for (int i = 0; i < size; ++i) {
     for(int j = 0; j < 32;j++){
         vector[i].data[j] = data;
     }

  }
}

static void dump_error(const char *str, cl_int status) {
  printf("%s\n", str);
  printf("Error code: %d\n", status);
}

// free the resources allocated during initialization
static void freeResources() {

  if(kernel[K_LOAD_DATA]) 
    clReleaseKernel(kernel[K_LOAD_DATA]); 
  if(kernel[K_STORE_DATA]) 
    clReleaseKernel(kernel[K_STORE_DATA]); 
  if(program) 
    clReleaseProgram(program);
  if(queue[K_LOAD_DATA]) 
    clReleaseCommandQueue(queue[K_LOAD_DATA]);
  if(queue[K_STORE_DATA]) 
    clReleaseCommandQueue(queue[K_STORE_DATA]);
  if(hdin) 
   clSVMFreeAltera(context,hdin);  
  if(hdout) 
   clSVMFreeAltera(context,hdout);  
  if(context) 
    clReleaseContext(context);
}

void cleanup(){}

int main(int argc, char *argv[]) {
    
  cl_uint num_platforms;
  cl_uint num_devices;
  
  if ( argc >= 2 ) /* argc should be  >2 for correct execution */
  {
      vectorSize = atoi(argv[1]);
  }    

  // get the platform ID
  status = clGetPlatformIDs(1, &platform, &num_platforms);
  if(status != CL_SUCCESS) {
    dump_error("Failed clGetPlatformIDs.", status);
    freeResources();
    return 1;
  }
  
  if(num_platforms != 1) {
    printf("Found %d platforms!\n", num_platforms);
    freeResources();
    return 1;
  }

  // get the device ID
  status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 1, &device, &num_devices);
  if(status != CL_SUCCESS) {
    dump_error("Failed clGetDeviceIDs.", status);
    freeResources();
    return 1;
  }
  
  if(num_devices != 1) {
    printf("Found %d devices!\n", num_devices);
    freeResources();
    return 1;
  }

  // create a context
  context = clCreateContext(0, 1, &device, NULL, NULL, &status);
  if(status != CL_SUCCESS) {
    dump_error("Failed clCreateContext.", status);
    freeResources();
    return 1;
  }
    
  printf("Creating host buffers.\n");
  // allocate and initialize the input vectors for gouraud
  hdin = (cl_t*)clSVMAllocAltera(context, 0, vectorSize*sizeof(cl_t), 1024);
  hdout = (cl_t*)clSVMAllocAltera(context, 0, vectorSize*sizeof(cl_t), 1024);
  
  double numBytes = vectorSize*sizeof(cl_t);
  
  if(!hdin || !hdout) {
     dump_error("Failed to allocate buffers.", status);
     freeResources();
     return 1;   
  }
  initializeVector(hdin,0xFFFF,vectorSize);
  initializeVector(hdout,0x0000,vectorSize);
  // create a command queue
  queue[K_LOAD_DATA] = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
  if(status != CL_SUCCESS) {
     dump_error("Failed clCreateCommandQueue.", status);
     freeResources();
     return 1;
  }
  queue[K_STORE_DATA] = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
  if(status != CL_SUCCESS) {
     dump_error("Failed clCreateCommandQueue.", status);
     freeResources();
     return 1;
  }
  
  // create the program
  cl_int kernel_status;  
  size_t binsize = 0;
  unsigned char * binary_file = loadBinaryFile("../../device/loopback.aocx", &binsize);
  if(!binary_file) {
    dump_error("Failed loadBinaryFile.", status);
    freeResources();
    return 1;
  }
  program = clCreateProgramWithBinary(context, 1, &device, &binsize, (const unsigned char**)&binary_file, &kernel_status, &status);
  if(status != CL_SUCCESS) {
    dump_error("Failed clCreateProgramWithBinary.", status);
    freeResources();
    return 1;
  }
  delete [] binary_file;
  // build the program
  status = clBuildProgram(program, 0, NULL, "", NULL, NULL);
  if(status != CL_SUCCESS) {
    dump_error("Failed clBuildProgram.", status);
    freeResources();
    return 1;
  }
  printf("Creating loopback kernel\n");
  {
      // create the kernel
      kernel[K_LOAD_DATA] = clCreateKernel(program,kernel_name[K_LOAD_DATA], &status);
      if(status != CL_SUCCESS) {
        dump_error("Failed clCreateKernel.", status);
        freeResources();
        return 1;
      }
      
      kernel[K_STORE_DATA] = clCreateKernel(program,kernel_name[K_STORE_DATA], &status);
      if(status != CL_SUCCESS) {
        dump_error("Failed clCreateKernel.", status);
        freeResources();
        return 1;
      }
      
      high_resolution_clock::time_point s;
      duration<double> diff{};
      s = high_resolution_clock::now();
      
      // set the arguments          
      status = clSetKernelArgSVMPointerAltera(kernel[K_LOAD_DATA], 0, (void*)hdin);
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 0.", status);
        freeResources();
        return 1;
      }
      
      cl_int arg_2 = vectorSize;
      status = clSetKernelArg(kernel[K_LOAD_DATA],1, sizeof(cl_int), &(arg_2));
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 1.", status);
        freeResources();
        return 1;
      }
      
      status = clSetKernelArgSVMPointerAltera(kernel[K_STORE_DATA], 1, (void*)hdout);
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 0.", status);
        freeResources();
        return 1;
      }
      
      status = clSetKernelArg(kernel[K_STORE_DATA],1, sizeof(cl_int), &(arg_2));
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 1.", status);
        freeResources();
        return 1;
      }
      
      printf("Launching the kernel...\n");
      
      status = clEnqueueSVMMap(queue[K_LOAD_DATA], CL_TRUE,  CL_MAP_READ | CL_MAP_WRITE,(void *)hdin,vectorSize*sizeof(cl_t), 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMMap", status);
        freeResources();
        return 1;
      } 
      status = clEnqueueSVMMap(queue[K_STORE_DATA], CL_TRUE,  CL_MAP_READ | CL_MAP_WRITE,(void *)hdout,vectorSize*sizeof(cl_t), 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMMap", status);
        freeResources();
        return 1;
      }
      
      status = clEnqueueTask(queue[K_LOAD_DATA], kernel[K_LOAD_DATA], 0, NULL, NULL);
      if (status != CL_SUCCESS) {
        dump_error("Failed to launch kernel.", status);
        freeResources();
        return 1;
      }
     status = clEnqueueTask(queue[K_STORE_DATA], kernel[K_STORE_DATA], 0, NULL, NULL);
      if (status != CL_SUCCESS) {
        dump_error("Failed to launch kernel.", status);
        freeResources();
        return 1;
      }
      status = clEnqueueSVMUnmap(queue[K_LOAD_DATA], (void *)hdin, 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMUnmap", status);
        freeResources();
        return 1;
      }   
      
      status = clEnqueueSVMUnmap(queue[K_STORE_DATA], (void *)hdout, 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMUnmap", status);
        freeResources();
        return 1;
      }   
      
      clFinish(queue[K_LOAD_DATA]);
      clFinish(queue[K_STORE_DATA]);
      
      diff = high_resolution_clock::now() - s;
      printf("Execution Time: %.4f ms\n",diff.count() * 1000);
      double time = diff.count();    
      bw = (numBytes / (time))/(1024*1024) ;
      printf("Read/Write Bandwidth = %.0f MB/s\n", bw);
      printf("Kernel execution is complete.\n");
      // Verify the output
      for(int i= 0;i < vectorSize;i++){
         for(int j = 0;j < 32;j++){
             if(hdin[i].data[j] != hdout[i].data[j]){
                  printf("Error found: hdin[%d].data[%d] != hdout[%d].data[%d], expected %d founded: %d",i,j,i,j,hdin[i].data[j],hdout[i].data[j]); 
                  break;
            }
         }
         printf("\n");
      }
  }
  freeResources();

  return 0;
}
