#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <chrono>
// ACL specific includes
#include "CL/opencl.h"
#include "AOCLUtils/aocl_utils.h"

using namespace aocl_utils;
using namespace std;
using namespace std::chrono;

static const size_t V = 32;
static size_t vectorSize = 1000000;
static char *kernel_name[2] =  {"load_data","store_data"};
enum KERNELS {K_LOAD_DATA, K_STORE_DATA};

double bw;

// ACL runtime configuration
static cl_platform_id platform;
static cl_device_id device;
static cl_context context;
static cl_command_queue queue[2];
static cl_kernel kernel[2];
static cl_program program;
static cl_int status;

typedef struct cache_line{
   unsigned short data[32]; 
}cl_t;

// input and output vectors
static cl_t *hdin, *hdout;

static void initializeVector(cl_t * vector, unsigned short data, int size);

static void dump_error(const char *str, cl_int status);

// free the resources allocated during initialization
static void freeResources();

void cleanup();

int main(int argc, char *argv[]);

