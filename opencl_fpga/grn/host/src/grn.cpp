#include <grn.h>


static void initializeVector(unsigned int* vector, unsigned int data, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = data;
  }
}

static void initializeVector_seq(unsigned int* vector, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = i;
  }
}

static void initializeVector_seq_inv(unsigned int* vector, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = size- i;
  }
}

static void dump_error(const char *str, cl_int status) {
  printf("%s\n", str);
  printf("Error code: %d\n", status);
}

// free the resources allocated during initialization
static void freeResources() {

  if(kernel) 
    clReleaseKernel(kernel);  
  if(kernel_read) 
    clReleaseKernel(kernel_read);  
  if(kernel_write) 
    clReleaseKernel(kernel_write);      
  if(program) 
    clReleaseProgram(program);
  if(queue) 
    clReleaseCommandQueue(queue);
  if(period_vet)
    clSVMFreeAltera(context,period_vet);
  if(transient_vet) 
   clSVMFreeAltera(context,transient_vet);  
  if(context) 
    clReleaseContext(context);
}

void cleanup(){}

int main(int argc, char *argv[]) {
    
  cl_uint num_platforms;
  cl_uint num_devices;
  int lines = vectorSize/V;
  if ( argc > 2 ) /* argc should be  > 2 for correct execution */
  {
      inicio = atoi(argv[1]);
      fim  = atoi(argv[2]);
      vectorSize = fim - inicio;
      lines = vectorSize/V;
  }    

  // get the platform ID
  status = clGetPlatformIDs(1, &platform, &num_platforms);
  if(status != CL_SUCCESS) {
    dump_error("Failed clGetPlatformIDs.", status);
    freeResources();
    return 1;
  }
  
  if(num_platforms != 1) {
    printf("Found %d platforms!\n", num_platforms);
    freeResources();
    return 1;
  }

  // get the device ID
  status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 1, &device, &num_devices);
  if(status != CL_SUCCESS) {
    dump_error("Failed clGetDeviceIDs.", status);
    freeResources();
    return 1;
  }
  
  if(num_devices != 1) {
    printf("Found %d devices!\n", num_devices);
    freeResources();
    return 1;
  }

  // create a context
  context = clCreateContext(0, 1, &device, NULL, NULL, &status);
  if(status != CL_SUCCESS) {
    dump_error("Failed clCreateContext.", status);
    freeResources();
    return 1;
  }
    
  printf("Creating host buffers.\n");
  // allocate and initialize the input vectors for gouraud
  period_vet = (unsigned int*)clSVMAllocAltera(context, 0, vectorSize*sizeof(unsigned int), 1024);
  transient_vet = (unsigned int*)clSVMAllocAltera(context, 0, vectorSize*sizeof(unsigned int), 1024);
  
  double numBytes = vectorSize*sizeof(unsigned int)*2;
  
  if(!period_vet || !transient_vet) {
     dump_error("Failed to allocate buffers.", status);
     freeResources();
     return 1;   
  }
  
  initializeVector(period_vet,0,vectorSize);
  initializeVector(transient_vet,0,vectorSize);
  
  // create a command queue
  queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
  if(status != CL_SUCCESS) {
     dump_error("Failed clCreateCommandQueue.", status);
     freeResources();
     return 1;
  }
  
  // create the program
  cl_int kernel_status;  
  size_t binsize = 0;
  unsigned char * binary_file = loadBinaryFile("../../device/grn.aocx", &binsize);
  if(!binary_file) {
    dump_error("Failed loadBinaryFile.", status);
    freeResources();
    return 1;
  }
  program = clCreateProgramWithBinary(context, 1, &device, &binsize, (const unsigned char**)&binary_file, &kernel_status, &status);
  if(status != CL_SUCCESS) {
    dump_error("Failed clCreateProgramWithBinary.", status);
    freeResources();
    return 1;
  }
  delete [] binary_file;
  // build the program
  status = clBuildProgram(program, 0, NULL, "", NULL, NULL);
  if(status != CL_SUCCESS) {
    dump_error("Failed clBuildProgram.", status);
    freeResources();
    return 1;
  }
  printf("Creating GRN kernel\n");
  {
      // create the kernel
      kernel = clCreateKernel(program,kernel_name, &status);
      
      if(status != CL_SUCCESS) {
        dump_error("Failed clCreateKernel.", status);
        freeResources();
        return 1;
      }
      
      high_resolution_clock::time_point s;
      duration<double> diff{};
      s = high_resolution_clock::now();
      
      // set the arguments          
      cl_int arg_0 = inicio;
      status = clSetKernelArg(kernel, 0, sizeof(cl_int), &(arg_0));
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 0.", status);
        freeResources();
        return 1;
      }
      cl_int arg_1 = fim;
      status = clSetKernelArg(kernel, 1, sizeof(cl_int), &(arg_1));
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 1.", status);
        freeResources();
        return 1;
      }
      
      status = clSetKernelArgSVMPointerAltera(kernel, 2, (void*)period_vet);
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 2.", status);
        freeResources();
        return 1;
      }
      
      status = clSetKernelArgSVMPointerAltera(kernel, 3, (void*)transient_vet);
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 3.", status);
        freeResources();
        return 1;
      }
      
      printf("Launching the kernel...\n");
      
      status = clEnqueueSVMMap(queue, CL_TRUE,  CL_MAP_READ | CL_MAP_WRITE,(void *)period_vet,vectorSize*sizeof(unsigned int), 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMMap", status);
        freeResources();
        return 1;
      } 
      
      status = clEnqueueSVMMap(queue, CL_TRUE,  CL_MAP_READ | CL_MAP_WRITE,(void *)transient_vet,vectorSize*sizeof(unsigned int), 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMMap", status);
        freeResources();
        return 1;
      } 
      
      status = clEnqueueTask(queue, kernel, 0, NULL, NULL);
      if (status != CL_SUCCESS) {
        dump_error("Failed to launch kernel.", status);
        freeResources();
        return 1;
      }
      
      status = clEnqueueSVMUnmap(queue, (void *)period_vet, 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMUnmap", status);
        freeResources();
        return 1;
      }   
      status = clEnqueueSVMUnmap(queue, (void *)transient_vet, 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMUnmap", status);
        freeResources();
        return 1;
      }  
      
      clFinish(queue);
      
      diff = high_resolution_clock::now() - s;
      printf("Execution Time: %.4f ms\n",diff.count() * 1000);
      double time = diff.count();    
      bw = numBytes / (time) ;
      printf("Processed %d states in %.4f s\n", vectorSize, time);
      printf("Read/Write Bandwidth = %.0f MB/s\n", bw/(1024*1024));
      printf("Kernel execution is complete.\n");

  }
  freeResources();

  return 0;
}
