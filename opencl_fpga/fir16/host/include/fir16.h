#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <chrono>
// ACL specific includes
#include "CL/opencl.h"
#include "AOCLUtils/aocl_utils.h"

using namespace aocl_utils;
using namespace std;
using namespace std::chrono;

void initializeVector(unsigned short* vector, unsigned short data, int size);

void initializeVector_seq(unsigned short* vector, int size);

void initializeVector_seq_inv(unsigned short* vector, int size);

void dump_error(const char *str, cl_int status);

// free the resources allocated during initialization
void freeResources();

void cleanup();

int main(int argc, char *argv[]);
