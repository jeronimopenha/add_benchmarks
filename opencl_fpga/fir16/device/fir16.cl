
#define COPIES 64

__kernel void 
__attribute__((task))
fir16 (__global const unsigned short * restrict din,
       __global const unsigned short * restrict coef,
       __global unsigned short * restrict dout,
       int n
      ){     
    
    unsigned short ai[COPIES][16];
    unsigned short coef_hw[COPIES][16];
    unsigned short fir[COPIES];

    #pragma unroll COPIES
    for (int i = 0; i < COPIES; i++) {
        #pragma unroll 16
        for (int j = 0; j < 16; j++) {
            coef_hw[i][j] = coef[i * COPIES + j];
        }
    }
    #pragma unroll COPIES
    for (int i = 0; i < COPIES; i++) {
        for (int j = 0; j < n; j++) {
            fir[i] = 0;
            ai[i][0] = din[i*n + j];
            #pragma unroll 15
            for (int k = 15; k > 0; k--) {
                fir[i] += ai[i][k] * coef_hw[i][k];
                ai[i][k] = ai[i][k - 1];
            }
            fir[i] += ai[i][0] * coef_hw[i][0];
            if (j >= 15) {
                dout[i*n + (j - 15)] = fir[i];
            }
        }
    }
}
