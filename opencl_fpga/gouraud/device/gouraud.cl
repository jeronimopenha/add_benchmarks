
__attribute__((task))
__kernel void 
gouraud (
         unsigned short rd,
         unsigned short r,
         unsigned short gd,
         unsigned short g,
         unsigned short bd,
         unsigned short b,
       __global unsigned short * restrict p,
       int n
){
    
  unsigned int mask = 0xF800F800;
  for(int i = 0; i < n; i=i+32) {
      #pragma unroll 32
      for(int j = 0; j < 32; j++){
          unsigned short r_i = r, g_i = g, b_i = b; 
          r_i += rd;
          g_i += gd; 
          b_i += bd;
          p[i*32 + j] = (r_i & mask) + ((g_i & mask) >> 5) + ((b_i & mask) >> 10);
      }
  }
}
