#include <gouraud.h>


static void initializeVector(unsigned short* vector, unsigned short data, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = data;
  }
}

static void initializeVector_seq(unsigned short* vector, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = i;
  }
}

static void initializeVector_seq_inv(unsigned short* vector, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = size- i;
  }
}

static void dump_error(const char *str, cl_int status) {
  printf("%s\n", str);
  printf("Error code: %d\n", status);
}

// free the resources allocated during initialization
static void freeResources() {

  if(kernel) 
    clReleaseKernel(kernel);  
  if(kernel_read) 
    clReleaseKernel(kernel_read);  
  if(kernel_write) 
    clReleaseKernel(kernel_write);      
  if(program) 
    clReleaseProgram(program);
  if(queue) 
    clReleaseCommandQueue(queue);
  if(hdout) 
   clSVMFreeAltera(context,hdout);  
  if(context) 
    clReleaseContext(context);
}

void cleanup(){}

int main(int argc, char *argv[]) {
    
  cl_uint num_platforms;
  cl_uint num_devices;
  int lines = vectorSize/V;
  if ( argc >= 2 ) /* argc should be  >2 for correct execution */
  {
      vectorSize = atoi(argv[1]);
      lines = atoi(argv[1])/V;
  }    

  // get the platform ID
  status = clGetPlatformIDs(1, &platform, &num_platforms);
  if(status != CL_SUCCESS) {
    dump_error("Failed clGetPlatformIDs.", status);
    freeResources();
    return 1;
  }
  
  if(num_platforms != 1) {
    printf("Found %d platforms!\n", num_platforms);
    freeResources();
    return 1;
  }

  // get the device ID
  status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 1, &device, &num_devices);
  if(status != CL_SUCCESS) {
    dump_error("Failed clGetDeviceIDs.", status);
    freeResources();
    return 1;
  }
  
  if(num_devices != 1) {
    printf("Found %d devices!\n", num_devices);
    freeResources();
    return 1;
  }

  // create a context
  context = clCreateContext(0, 1, &device, NULL, NULL, &status);
  if(status != CL_SUCCESS) {
    dump_error("Failed clCreateContext.", status);
    freeResources();
    return 1;
  }
    
  printf("Creating host buffers.\n");
  // allocate and initialize the input vectors for gouraud
  hdout = (unsigned short*)clSVMAllocAltera(context, 0, vectorSize*sizeof(unsigned short), 1024);
  
  double numBytes = vectorSize*sizeof(unsigned short);
  
  if(!hdout) {
     dump_error("Failed to allocate buffers.", status);
     freeResources();
     return 1;   
  }
  initializeVector(hdout,0x0000,vectorSize);
  // create a command queue
  queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
  if(status != CL_SUCCESS) {
     dump_error("Failed clCreateCommandQueue.", status);
     freeResources();
     return 1;
  }
  
  // create the program
  cl_int kernel_status;  
  size_t binsize = 0;
  unsigned char * binary_file = loadBinaryFile("../../device/gouraud.aocx", &binsize);
  if(!binary_file) {
    dump_error("Failed loadBinaryFile.", status);
    freeResources();
    return 1;
  }
  program = clCreateProgramWithBinary(context, 1, &device, &binsize, (const unsigned char**)&binary_file, &kernel_status, &status);
  if(status != CL_SUCCESS) {
    dump_error("Failed clCreateProgramWithBinary.", status);
    freeResources();
    return 1;
  }
  delete [] binary_file;
  // build the program
  status = clBuildProgram(program, 0, NULL, "", NULL, NULL);
  if(status != CL_SUCCESS) {
    dump_error("Failed clBuildProgram.", status);
    freeResources();
    return 1;
  }
  printf("Creating Gouraud kernel\n");
  {
      // create the kernel
      kernel = clCreateKernel(program,kernel_name, &status);
      
      if(status != CL_SUCCESS) {
        dump_error("Failed clCreateKernel.", status);
        freeResources();
        return 1;
      }
      
      high_resolution_clock::time_point s;
      duration<double> diff{};
      s = high_resolution_clock::now();
      
      // set the arguments          
      cl_short arg_0 = rd;
      status = clSetKernelArg(kernel, 0, sizeof(cl_short), &(arg_0));
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 0.", status);
        freeResources();
        return 1;
      }
      cl_short arg_1 = r;
      status = clSetKernelArg(kernel, 1, sizeof(cl_short), &(arg_1));
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 1.", status);
        freeResources();
        return 1;
      }
      
      cl_short arg_2 = gd;
      status = clSetKernelArg(kernel, 2, sizeof(cl_short), &(arg_2));
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 2.", status);
        freeResources();
        return 1;
      }
      
      cl_short arg_3 = g;
      status = clSetKernelArg(kernel, 3, sizeof(cl_short), &(arg_3));
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 3.", status);
        freeResources();
        return 1;
      }
      
      cl_short arg_4 = bd;
      status = clSetKernelArg(kernel, 4, sizeof(cl_short), &(arg_4));
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 4.", status);
        freeResources();
        return 1;
      }
      
      cl_short arg_5 = b;
      status = clSetKernelArg(kernel, 5, sizeof(cl_short), &(arg_5));
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 5.", status);
        freeResources();
        return 1;
      }

      status = clSetKernelArgSVMPointerAltera(kernel, 6, (void*)hdout);
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 6.", status);
        freeResources();
        return 1;
      }
      
      cl_int arg_7 = vectorSize;
      status = clSetKernelArg(kernel, 7, sizeof(cl_int), &(arg_7));
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 7.", status);
        freeResources();
        return 1;
      }
      
      printf("Launching the kernel...\n");
      
      status = clEnqueueSVMMap(queue, CL_TRUE,  CL_MAP_READ | CL_MAP_WRITE,(void *)hdout,vectorSize*sizeof(unsigned short), 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMMap", status);
        freeResources();
        return 1;
      } 
      
      status = clEnqueueTask(queue, kernel, 0, NULL, NULL);
      if (status != CL_SUCCESS) {
        dump_error("Failed to launch kernel.", status);
        freeResources();
        return 1;
      }
      
      status = clEnqueueSVMUnmap(queue, (void *)hdout, 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMUnmap", status);
        freeResources();
        return 1;
      }   
      
      clFinish(queue);
      
      diff = high_resolution_clock::now() - s;
      printf("Execution Time: %.4f ms\n",diff.count() * 1000);
      double time = diff.count();    
      bw = numBytes / (time) ;
      printf("Processed %d unsigned shorts in %.4f s\n", vectorSize, time);
      printf("Read/Write Bandwidth = %.0f MB/s\n", bw/(1024*1024));
      printf("Kernel execution is complete.\n");
      // Verify the output
      if(vectorSize <= 100){
         for(int i = 0;i < vectorSize;i++){
            printf("%d ",hdout[i]);
         }
         printf("\n");
      }
  }
  freeResources();

  return 0;
}
