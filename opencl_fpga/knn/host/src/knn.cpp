#include <knn.h>

void initializeVector(unsigned short* vector, unsigned short data, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = data;
  }
}

void initializeVector_seq(unsigned short* vector, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = i;
  }
}

void initializeVector_seq_inv(unsigned short* vector, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = size- i;
  }
}

void dump_error(const char *str, cl_int status) {
  printf("%s\n", str);
  printf("Error code: %d\n", status);
}

// free the resources allocated during initialization
void freeResources() {

  if(kernel) 
    clReleaseKernel(kernel);  
  if(kernel_read) 
    clReleaseKernel(kernel_read);  
  if(kernel_write) 
    clReleaseKernel(kernel_write);      
  if(program) 
    clReleaseProgram(program);
  if(queue) 
    clReleaseCommandQueue(queue);
  if(vetorPontos) 
   clSVMFreeAltera(context,vetorPontos);
  if(centroide)
   clSVMFreeAltera(context,centroide);
  if(saida) 
   clSVMFreeAltera(context,saida);  
  if(context) 
    clReleaseContext(context);
}

void cleanup(){}

int main(int argc, char *argv[]) {
    
  cl_uint num_platforms;
  cl_uint num_devices;

  if ( argc >= 2 ) /* argc should be  > 2 for correct execution */
  {
      numPontos = atoi(argv[1]);
  }    

  // get the platform ID
  status = clGetPlatformIDs(1, &platform, &num_platforms);
  if(status != CL_SUCCESS) {
    dump_error("Failed clGetPlatformIDs.", status);
    freeResources();
    return 1;
  }
  
  if(num_platforms != 1) {
    printf("Found %d platforms!\n", num_platforms);
    freeResources();
    return 1;
  }

  // get the device ID
  status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 1, &device, &num_devices);
  if(status != CL_SUCCESS) {
    dump_error("Failed clGetDeviceIDs.", status);
    freeResources();
    return 1;
  }
  
  if(num_devices != 1) {
    printf("Found %d devices!\n", num_devices);
    freeResources();
    return 1;
  }

  // create a context
  context = clCreateContext(0, 1, &device, NULL, NULL, &status);
  if(status != CL_SUCCESS) {
    dump_error("Failed clCreateContext.", status);
    freeResources();
    return 1;
  }
    
  printf("Creating host buffers.\n");   
  // allocate and initialize the input vectors for knn
  vetorPontos =  (unsigned short*)clSVMAllocAltera(context, 0, numPontos*DIM*sizeof(unsigned short), 1024); 
  centroide = (unsigned short*)clSVMAllocAltera(context, 0, DIM*sizeof(unsigned short), 1024);
  saida = (unsigned short*)clSVMAllocAltera(context, 0,DIM*K*sizeof(unsigned short), 1024);
  
  double numBytes = 0;
  
  if(!vetorPontos || !centroide || !saida) {
     dump_error("Failed to allocate buffers.", status);
     freeResources();
     return 1;   
  }
  initializeVector(vetorPontos,2,numPontos*DIM);
  initializeVector(saida,0,DIM*K);
  initializeVector_seq_inv(centroide,DIM);
  
  // create a command queue
  queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
  if(status != CL_SUCCESS) {
     dump_error("Failed clCreateCommandQueue.", status);
     freeResources();
     return 1;
  }
  
  // create the program
  cl_int kernel_status;  
  size_t binsize = 0;
  unsigned char * binary_file = loadBinaryFile("../../device/knn.aocx", &binsize);
  if(!binary_file) {
    dump_error("Failed loadBinaryFile.", status);
    freeResources();
    return 1;
  }
  program = clCreateProgramWithBinary(context, 1, &device, &binsize, (const unsigned char**)&binary_file, &kernel_status, &status);
  if(status != CL_SUCCESS) {
    dump_error("Failed clCreateProgramWithBinary.", status);
    freeResources();
    return 1;
  }
  delete [] binary_file;
  // build the program
  status = clBuildProgram(program, 0, NULL, "", NULL, NULL);
  if(status != CL_SUCCESS) {
    dump_error("Failed clBuildProgram.", status);
    freeResources();
    return 1;
  }
  printf("Creating Knn kernel\n");
  {
      // create the kernel
      kernel = clCreateKernel(program,kernel_name, &status);
      
      if(status != CL_SUCCESS) {
        dump_error("Failed clCreateKernel.", status);
        freeResources();
        return 1;
      }
      
      high_resolution_clock::time_point s;
      duration<double> diff{};
      s = high_resolution_clock::now();
      
      // set the arguments
      status = clSetKernelArgSVMPointerAltera(kernel, 0, (void*)vetorPontos);
      if(status != CL_SUCCESS) {
        dump_error("Failed set arg 0.", status);
        return 1;
      }
      status = clSetKernelArgSVMPointerAltera(kernel, 1, (void*)centroide);
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 1.", status);
        freeResources();
        return 1;
      }
      
      status = clSetKernelArgSVMPointerAltera(kernel, 2, (void*)saida);
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 2.", status);
        freeResources();
        return 1;
      }
      
      cl_int arg_3 = numPontos;
      status = clSetKernelArg(kernel, 3, sizeof(cl_int), &(arg_3));
      if(status != CL_SUCCESS) {
        dump_error("Failed Set arg 3.", status);
        freeResources();
        return 1;
      }
      
      printf("Launching the kernel...\n");
      
      status = clEnqueueSVMMap(queue, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE,(void *)vetorPontos,numPontos*DIM*sizeof(unsigned short), 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMMap", status);
        freeResources();
        return 1;
      }
      status = clEnqueueSVMMap(queue, CL_TRUE,  CL_MAP_READ | CL_MAP_WRITE,(void *)centroide,DIM*sizeof(unsigned short), 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMMap", status);
        freeResources();
        return 1;
      }   
      
      status = clEnqueueSVMMap(queue, CL_TRUE,  CL_MAP_READ | CL_MAP_WRITE,(void *)saida,DIM*K*sizeof(unsigned short), 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMMap", status);
        freeResources();
        return 1;
      } 
      
      status = clEnqueueTask(queue, kernel, 0, NULL, NULL);
      if (status != CL_SUCCESS) {
        dump_error("Failed to launch kernel.", status);
        freeResources();
        return 1;
      }
      
      status = clEnqueueSVMUnmap(queue, (void *)vetorPontos, 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMUnmap", status);
        freeResources();
        return 1;
      }
      
      status = clEnqueueSVMUnmap(queue, (void *)centroide, 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMUnmap", status);
        freeResources();
        return 1;
      }
      status = clEnqueueSVMUnmap(queue, (void *)saida, 0, NULL, NULL); 
      if(status != CL_SUCCESS) {
        dump_error("Failed clEnqueueSVMUnmap", status);
        freeResources();
        return 1;
      }   
      
      clFinish(queue);
      
      diff = high_resolution_clock::now() - s;
      printf("Execution Time: %.4f ms\n",diff.count() * 1000);
      double time = diff.count();    
      bw = numBytes / (time) ;
      printf("Processed %d unsigned shorts in %.4f s\n", numPontos, time);
      printf("Read/Write Bandwidth = %.0f MB/s\n", bw/(1024*1024));
      printf("Kernel execution is complete.\n");
  }

  freeResources();

  return 0;
}
