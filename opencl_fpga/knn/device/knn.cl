
#define DIM 4
#define K 3


__kernel void 
__attribute__((task))
knn(__global const short * restrict vectorP,
              __global const short * restrict centroid, 
              __global short * restrict vectorOut, 
              int numP) {

    short distances[4][K];
    short vectorOutHw[K * DIM * 4];
    
    #pragma unroll K 
    for (int i = 0; i < K; i++) {
        #pragma unroll 4
        for(int j = 0; j < 4; j++){
            distances[j][i] = 0x7fff;
        }
    }

    for (int i = 0; i < numP; i = i + 4) {
        #pragma unroll 4
        for(int j = 0; j < 4; j++){
            short dimTmp[DIM];
            short tmp;
            short sum = 0;
            #pragma unroll DIM
            for (int c = 0; c < DIM; c++) {
                dimTmp[c] = vectorP[((i + j) * DIM) + c];
                tmp = vectorP[((i + j) * DIM) + c] - centroid[c];
                sum += (tmp >= 0) ? tmp : (short) tmp * -1;
            }

            #pragma unroll K
            for (int y = 0; y < K; y++) {
                if (distances[y] > sum) {
                    tmp = distances[y];
                    distances[y] = sum;
                    sum = tmp;
                    #pragma unroll DIM
                    for (c = 0; c < DIM; c++) {
                        tmp = vectorOutHw[(y + (j * K)) * DIM + c]; 
                        vectorOutHw[(y + (j * K)) * DIM + c] = dimTmp[c]; 
                        dimTmp[c] = tmp;
                    }
                }
            }
        }
    }
    
    #pragma unroll (K * DIM * 4)
    for(int i =0; i < K * DIM * 4 ; i++){
        vectorOut[i] = vectorOutHw[i];
    }
    
}
