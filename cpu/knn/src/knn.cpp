#include <stdio.h>
#include <stdlib.h>

#define DIM 4
#define K 3

void knn(short *vectorP, short *centroid, short *vectorOut, int numP) {

    short distances[4][K];
    short vectorOutHw[K * DIM * 4];
    
    #pragma unroll K 
    for (int i = 0; i < K; i++) {
        #pragma unroll 4
        for(int j = 0; j < 4; j++){
            distances[j][i] = 0x7fff;
        }
    }

    for (int i = 0; i < numP; i = i + 4) {
        #pragma unroll 4
        for(int j = 0; j < 4; j++){
            short dimTmp[DIM];
            short tmp;
            short sum = 0;
            #pragma unroll DIM
            for (int c = 0; c < DIM; c++) {
                dimTmp[c] = vectorP[((i + j) * DIM) + c];
                tmp = vectorP[((i + j) * DIM) + c] - centroid[c];
                sum += (tmp >= 0) ? tmp : (short) tmp * -1;
            }

            #pragma unroll K
            for (int y = 0; y < K; y++) {
                if (distances[j][y] > sum) {
                    tmp = distances[j][y];
                    distances[j][y] = sum;
                    sum = tmp;
                    #pragma unroll DIM
                    for (int c = 0; c < DIM; c++) {
                        tmp = vectorOutHw[(y + (j * K)) * DIM + c]; 
                        vectorOutHw[(y + (j * K)) * DIM + c] = dimTmp[c]; 
                        dimTmp[c] = tmp;
                    }
                }
            }
        }
    }
    
    #pragma unroll (K * DIM * 4)
    for(int i =0; i < K * DIM * 4 ; i++){
        vectorOut[i] = vectorOutHw[i];
    }
    
}

int main() {
    int i, j, numPontos = 10;

    //short vetorPontos[DIM][numPontos], centroide[DIM] , saida[DIM][K];
    short *vetorPontos = (short *) malloc(sizeof(short) * DIM * numPontos);
    short *saida = (short *) malloc(sizeof(short) * DIM * K);
    short *centroide = (short *) malloc(sizeof(short) * DIM);

    //criação dos pontos
    for (i = 0; i < numPontos; i++) {
        for (j = 0; j < DIM; j++) {
            vetorPontos[j*numPontos + i] = K * 3;
            printf("%d ",vetorPontos[j*numPontos+ i]);
        }
        printf("\n");
    }
    printf("--------------------------------------------------------------------------------\n");
    //inicialização dos centroides
    for (j = 0; j < DIM; j++) {
        centroide[j] = 0;
    }


    //alocação dos pontos desejados
    for (i = 0; i < K; i++) {
        for (j = 0; j < DIM; j++) {
            vetorPontos[j * K +(numPontos - i - 1)] = K - i;
            printf("%d ",vetorPontos[j * K +(numPontos - i - 1)]);
        }
        printf("\n");
    }
    printf("--------------------------------------------------------------------------------\n");
    knn(vetorPontos, centroide, saida, numPontos);

    for (i = 0; i < K; i++) {
        for (j = 0; j < DIM; j++) {
            printf("%d ",saida[j * DIM + i]);
        }
        printf("\n");
    }


}



//0 0 0 0 centroide

//1 1 1 1
//2 2 2 2
//3 3 3 3

//k*3 k*3 k*3 k*3

