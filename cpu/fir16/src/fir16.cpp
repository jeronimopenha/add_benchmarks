#include <fir16.h>

void fir16 ( const unsigned short *  din, const unsigned short * coef, unsigned short * dout, int n){
        
     unsigned short ai[16];
     unsigned short coef_hw[16];
     #pragma unroll 16
     for(int i = 0; i < 16; i++) {
        coef_hw[i] = coef[i];       
     }

     for (int i=0; i < n; i++){
         unsigned fir = 0;
         ai[0] = din[i];
         #pragma unroll 15
         for (int k=15; k > 0; k--){
             fir += ai[k] * coef_hw[k];
             ai[k] = ai[k-1];
         }
         
         fir += ai[0] * coef_hw[0];
         
         if (i >= 15){
             dout[i-15] = fir;
         }
     }
}

#define COPIES 4
void fir16_new(const unsigned short *din, const unsigned short *coef, unsigned short *dout,  int n ){  
     
     unsigned short ai[COPIES][16];    
     unsigned short coef_hw[COPIES][16];
     unsigned fir[COPIES];
     
     #pragma unroll COPIES
     for(int i = 0; i < COPIES; i++) {
         #pragma unroll 16
         for(int j = 0; j < 16; j++){
           coef_hw[i][j] = coef[i*COPIES + j];       
         }
     }
     #pragma unroll COPIES
     for(int i = 0;i < COPIES; i++){
        for (int j = 0; j < n; j++){
            fir[i] = 0;
            ai[i][0] = din[i*COPIES + j];
            #pragma unroll 15
            for (int k=15; k > 0; k--){
                fir[i] += ai[i][k] * coef_hw[i][k];
                ai[i][k] = ai[i][k-1];
            }
            fir[i] += ai[i][0] * coef_hw[i][0];
            if (j >= 15){
                dout[i*COPIES + (j-15)] = fir[i];
            }
         }
     }
}


void initializeVector(unsigned short* vector, unsigned short data, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = data;
  }
}

void initializeVector_seq(unsigned short* vector, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] =  i;
  }
}

void initializeVector_seq_inv(unsigned short* vector, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = size - i;
  }
}

int fir16_test(int argc, char *argv[]) {
    
  int vectorSize;

  if ( argc >= 2 ) {
      vectorSize = atoi(argv[1]);
  }
  else{
    printf("Args error!\n");
    exit(1);
  }

  unsigned short *din,*coef,*dout;
  
  din = (unsigned short *)malloc(sizeof(unsigned short)*vectorSize);
  coef = (unsigned short *)malloc(sizeof(unsigned short)*16);
  dout = (unsigned short *)malloc(sizeof(unsigned short)*(vectorSize));
    
  if(!din || !coef || !dout){
      printf("Malloc error!\n");
      exit(2);
  }
  
  initializeVector(din,2, vectorSize);
  initializeVector(dout,0,vectorSize);
  initializeVector_seq_inv(coef,16);
    
  fir16(din,coef,dout,vectorSize);
  // Verify the output
  if(vectorSize <= 100){
    for(int i = 0;i < vectorSize;i++){
       printf("%d ",dout[i]);
    }
    printf("\n");
  }
  
  free(din);
  free(coef);
  free(dout);
     
  return 0;
}   
int fir16_new_test(int argc, char *argv[]) {
    
  int vectorSize;

  if ( argc >= 2 ) {
      vectorSize = atoi(argv[1]);
  }
  else{
    printf("Args error!\n");
    exit(1);
  }

  unsigned short *din,*coef,*dout;
  
  din = (unsigned short *)malloc(sizeof(unsigned short)*vectorSize*COPIES);
  coef = (unsigned short *)malloc(sizeof(unsigned short)* 16 * COPIES);
  dout = (unsigned short *)malloc(sizeof(unsigned short)*(vectorSize*COPIES));
    
  if(!din || !coef || !dout){
      printf("Malloc error!\n");
      exit(2);
  }
  
  initializeVector(din,2, vectorSize*COPIES);
  initializeVector(dout,0,vectorSize*COPIES);
  initializeVector(coef,1,16*COPIES);
    
  fir16(din,coef,dout,vectorSize);
  // Verify the output
  for(int i = 0;i < COPIES;i++){
     if(vectorSize <= 100){
        for(int j = 0;j < vectorSize;j++){
            printf("%d ",dout[j]);
        }
        printf("\n----------------------------------------------------------------------------------------------------\n");
     }
  }
  
  free(din);
  free(coef);
  free(dout);
     
  return 0;
}  
int main(int argc, char *argv[]) {
    
   // fir16_test(argc,argv);
    
    fir16_new_test(argc,argv);
    
    return 0;
}
