
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void fir16 ( const unsigned short *  din, const unsigned short * coef, unsigned short * dout, int n);

void initializeVector(unsigned short* vector, unsigned short data, int size);

void initializeVector_seq(unsigned short* vector, int size);

void initializeVector_seq_inv(unsigned short* vector, int size);

int main(int argc, char *argv[]);
