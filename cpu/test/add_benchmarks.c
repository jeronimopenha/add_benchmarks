#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Gouraud params:
unsigned short rd = 1;
unsigned short r  = 1;
unsigned short gd = 1;
unsigned short g  = 1;
unsigned short bd = 1;
unsigned short b  = 1;

void paeth (const unsigned short *  a,
            const unsigned short *  b,
            const unsigned short *  c,
            unsigned short * out,
            int n
){
    
  int pas, pbs, pcs;
  bool test_1, test_2, test_3;
  for(int i = 0; i < n; i++) {
    pas = b[i] - c[i];
    pbs = a[i] - c[i];
    pcs = a[i] | b[i] - 2 * c[i];
    test_1 = abs(pas) <= abs(pbs);
    test_2 = abs(pas) <= abs(pcs);
    test_3 = abs(pbs) <= abs(pcs);
    if (test_1 && test_2) out[i] = a[i];
    else if (test_3) out[i] = b[i];
    else out[i] = c[i];
  }
}

void gouraud (
         unsigned short rd,
         unsigned short r,
         unsigned short gd,
         unsigned short g,
         unsigned short bd,
         unsigned short b,
         unsigned short * p,
         int n
){
    
  unsigned int mask = 0xF800F800;
  for(int i = 0; i < n; i++) {
    r += rd;
    g += gd; 
    b += bd;
    p[i] = (r & mask) + ((g & mask) >> 5) + ((b & mask) >> 10);
  }
}

void fir16 ( const unsigned short *  din, const unsigned short * coef, unsigned short * dout, int n){
        
     unsigned short ai[16];
     unsigned short coef_hw[16];
     #pragma unroll 16
     for(int i = 0; i < 16; i++) {
        coef_hw[i] = coef[i];       
     }
     
     for (int i=0; i < n; i++){
         unsigned fir = 0;
         ai[0] = din[i];
         #pragma unroll 15
         for (int k=15; k > 0; k--){
             fir += ai[k] * coef_hw[k];
             ai[k] = ai[k-1];
         }
         
         fir += ai[0] * coef_hw[0];
         
         if (i >= 15){
             dout[i-15] = fir;
         }
     }
}


void initializeVector(unsigned short* vector, unsigned short data, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = data;
  }
}

void initializeVector_seq(unsigned short* vector, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] =  i;
  }
}

void initializeVector_seq_inv(unsigned short* vector, int size) {
  for (int i = 0; i < size; ++i) {
    vector[i] = size - i;
  }
}

int main(int argc, char *argv[]) {
    
  int lines = 0;
  int vectorSize;
  char *kernel_name = "fir16";
  if ( argc >= 3 ) {
      kernel_name = argv[1];
      vectorSize = atoi(argv[2]);
      lines = atoi(argv[2])/32;
  }
  else{
    printf("Args error!\n");
    exit(1);
  }
  
  if(strcmp(kernel_name,"fir16") == 0){
     unsigned short *din,*coef,*dout;
     din = (unsigned short *)malloc(sizeof(unsigned short)*vectorSize);
     coef = (unsigned short *)malloc(sizeof(unsigned short)*16);
     dout = (unsigned short *)malloc(sizeof(unsigned short)*(vectorSize-16));
     if(!din || !coef || !dout){
         printf("Malloc error!\n");
         exit(2);
     }
     
     initializeVector(din,2, vectorSize);
     initializeVector(dout,0x0000,vectorSize-16);
     initializeVector_seq_inv(coef,16);
     
     fir16(din,coef,dout,vectorSize);
     // Verify the output
     if(vectorSize <= 100){
       for(int i = 0;i < vectorSize-16;i++){
          printf("%d ",dout[i]);
       }
       printf("\n");
     }
     
     free(din);
     free(coef);
     free(dout);
     
  }
  else if(strcmp(kernel_name,"paeth") == 0){
     unsigned short *din_a,*din_b, *din_c, *dout;
     din_a = (unsigned short *)malloc(sizeof(unsigned short)*vectorSize);
     din_b = (unsigned short *)malloc(sizeof(unsigned short)*vectorSize);
     din_c = (unsigned short *)malloc(sizeof(unsigned short)*vectorSize);
     dout  = (unsigned short *)malloc(sizeof(unsigned short)*(vectorSize));
     if(!din_a || !din_b || !din_c || !dout){
         printf("Malloc error!\n");
         exit(2);
     }
     
     initializeVector_seq(din_a, vectorSize);
     initializeVector_seq(din_b, vectorSize);
     initializeVector_seq(din_c, vectorSize);
     initializeVector(dout,0x0000,vectorSize);
         
     paeth(din_a,din_b,din_c,dout,vectorSize);
     // Verify the output
     if(vectorSize <= 100){
       for(int i = 0;i < vectorSize;i++){
          printf("%d ",dout[i]);
       }
       printf("\n");
     }
     
     free(din_a);
     free(din_b);
     free(din_c);
     free(dout);
  }
  else if(strcmp(kernel_name,"gouraud") == 0){
     unsigned short *din,*coef,*dout;
     dout = (unsigned short *)malloc(sizeof(unsigned short)*(vectorSize));
     if(!dout){
         printf("Malloc error!\n");
         exit(2);
     }
     
     initializeVector(dout,0x0000,vectorSize);

     gouraud(rd,r,gd,g,bd,b,dout,vectorSize);
     // Verify the output
     if(vectorSize <= 100){
       for(int i = 0;i < vectorSize;i++){
          printf("%d ",dout[i]);
       }
       printf("\n");
     }

     free(dout);
     
  }else{
    printf("Args error!\n");    
  }
  return 0;
}   
